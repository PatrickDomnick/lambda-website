'use strict'

const path = require('path')
const fastify = require('fastify')({ logger: { level: 'trace' } })

const app = fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public'),
})

if (require.main === module) {
  app.listen(3000, (err) => {   // called directly i.e. "node app"
    if (err) console.error(err)
    console.log('server listening on 3000')
  })
} else {
  module.exports = app // required as a module => executed on aws lambda
}